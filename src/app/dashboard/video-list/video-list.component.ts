import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Video } from '../video.types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {

  @Input() videos: Video[] = [];
  @Input() selectedVideo: Video;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onVideoClicked(video: Video) {
    const queryParams = {selectedId: video?.id};
    void this.router.navigate([], {queryParams});
  }
}
