import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnInit {
  group: FormGroup;

  regions = ['North America', 'Europe', 'Asia', 'All'];

  constructor(fb: FormBuilder) {
    this.group = fb.group({
      region: ['All']
    });
  }

  ngOnInit(): void {
  }

}
