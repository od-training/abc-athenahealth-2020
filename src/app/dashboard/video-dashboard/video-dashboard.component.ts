import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, share, switchMap } from 'rxjs/operators';
import { VideoDataService } from '../../video-data.service';
import { Video } from '../video.types';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {

  videoList: Observable<Video[]>;
  selectedVideo: Observable<Video>;

  constructor(svc: VideoDataService, route: ActivatedRoute) {
    this.videoList = svc.getVideos();

    this.selectedVideo = route.queryParamMap
      .pipe(
        map(params => params.get('selectedId')),
        switchMap(id => {
          return id ? svc.getVideo(id) : of(null);
        }),
        share()
      );

  }

  ngOnInit(): void {
  }

}
