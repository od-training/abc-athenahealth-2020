import { Component, Input, OnInit } from '@angular/core';
import { Video } from '../video.types';

@Component({
  selector: 'app-video-display',
  templateUrl: './video-display.component.html',
  styleUrls: ['./video-display.component.css']
})
export class VideoDisplayComponent implements OnInit {
  @Input() video: Video;

  constructor() { }

  ngOnInit(): void {
  }

}
