import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Video } from './dashboard/video.types';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  getVideos(): Observable<Video[]> {
    return this.http.get<Video[]>('https://api.angularbootcamp.com/videos')
      .pipe(
        tap(videos => console.table(videos)),
        map(videos => videos.slice(0, 2)),
        tap(videos => console.table(videos))
      );
  }

  getVideo(id: string): Observable<Video> {
    return this.http.get<Video>('https://api.angularbootcamp.com/videos/' + id);
  }
}


